# goblog-web

> 个人博客goblog前端

![license](https://img.shields.io/badge/license-GPL3.0-green.svg)

## 后端

[goblog](https://gitlab.com/xiayesuifeng/goblog.git)

## 编译

```
git clone https://gitlab.com/xiayesuifeng/goblog-web.git
npm install
npm build
```

[详细安装](https://gitlab.com/xiayesuifeng/goblog/blob/master/README.md)

## License

goblog-web is licensed under [GPLv3](LICENSE).
