import React, { Suspense, useEffect, useReducer, useState } from 'react'
import './index.css'
import Admin from './pages/admin/Admin'
import { CssBaseline, Snackbar } from '@mui/material'
import { Route, Routes } from 'react-router-dom'
import axios from 'axios'
import LoadingFrame from './component/LoadingFrame'
import { GlobalReducer } from './globalState/Reducer'
import { GlobalContext, initGlobalValue } from './globalState/Context'
import { CategoryApi } from './api/category'
import initPlugins from './plugins/plugins'

const Home = React.lazy(() => import('./pages/Home'))
const Login = React.lazy(() => import('./pages/Login'))
const Article = React.lazy(() => import('./pages/Article'))
const ArticleEditor = React.lazy(() => import('./pages/ArticleEditor'))

export default function App () {
    const [globalState, globalDispatch] = useReducer(GlobalReducer, initGlobalValue)

    const [snackBar, setSnackBar] = useState({open: false, message: ''})

    useEffect(() => {
        axios.get('/api/info')
            .then(r => {
                let info = {
                    title: r.data.name,
                    useCategory: r.data.useCategory,
                    logo: r.data.logo
                }

                if (info.logo !== 'none') {
                    document.querySelector('link[rel=\'shortcut icon\']').href = '/api/logo'
                }

                globalDispatch({type: 'SET_INFO', data: info})
            })

        initPlugins(globalDispatch)
    }, [])

    useEffect(() => {
        if (globalState.info.useCategory)
            CategoryApi.getCategory().then(data => globalDispatch({
                type: 'SET_CATEGORIES',
                data: data.categorys
            }))
    }, [globalState.info.useCategory])

    const showSnackBar = message => {
        setSnackBar({open: true, message: message})
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }

        setSnackBar({open: false, message: ''})
    }

    return (
        <div>
            <CssBaseline/>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={snackBar.open}
                onClose={handleClose}
                autoHideDuration={3000}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{snackBar.message}</span>}
            />
            <Suspense fallback={<LoadingFrame/>}>
                <GlobalContext.Provider value={{
                    ...globalState,
                    globalDispatch,
                    showSnackBar
                }}>
                    <Routes>
                        <Route path="/admin/*" element={<Admin />}/>
                        <Route path="/login" element={<Login />}/>
                        <Route path="/article/:id" element={<Article />}/>
                        <Route path="/articleEditor/:id" element={<ArticleEditor />}/>
                        <Route path="/articleEditor" element={<ArticleEditor />}/>
                        <Route path="/:type/:id" element={<Home />}/>
                        <Route path="/" element={<Home />}/>
                    </Routes>
                </GlobalContext.Provider>
            </Suspense>
        </div>
    )
}
