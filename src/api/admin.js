import { http } from './http'

export const AdminApi = {
    login (password) {
        return http.post('/api/login', {
            'password': password
        })
    },

    logout() {
        return http.post('/api/logout')
    },

    putAssets(blob) {
        let data = new FormData()
        data.append('assets', blob)

        return http.put('/api/assets', data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
    }
}