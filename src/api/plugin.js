import { http } from './http'
import axios from 'axios'

const baseUrl = '/api/plugin'

export const PluginApi = {
    getPlugin() {
        return http.get(baseUrl)
    },

    getAllPlugins() {
        return axios.get('https://xiayesuifeng.gitlab.io/goblog-plugins/plugins.json')
    },

    deletePlugin(name) {
        return axios.delete(`${baseUrl}/${name}`)
    }
}