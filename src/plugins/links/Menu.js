import { ListItemText, MenuItem, MenuList } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { LinkApi } from './api/links'
import ListSubheader from '@mui/material/ListSubheader'

export default function Menu () {
    const [links, setLinks] = useState([])

    useEffect(() => {
        LinkApi.getLinks().then(data => setLinks(data.links))
    }, [])

    return (
        <MenuList
            subheader={<ListSubheader>友链</ListSubheader>}>
            {links.map(link => (
                <MenuItem component='a' href={link.url} target="view_window">
                    <ListItemText primary={link.name}/>
                </MenuItem>
            ))}
        </MenuList>
    )
}
