import React, { useContext, useEffect, useState } from 'react'
import { Snackbar, Tooltip } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import axios from 'axios'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'
import IconButton from '@mui/material/IconButton'
import Fab from '@mui/material/Fab'
import AddIcon from '@mui/icons-material/Add'
import Paper from '@mui/material/Paper'
import { LinkApi } from './api/links'
import { GlobalContext } from '../../globalState/Context'
import Dialog from '@mui/material/Dialog'
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery'
import DialogTitle from '@mui/material/DialogTitle'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import CircularProgress from '@mui/material/CircularProgress'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(5),
        [theme.breakpoints.down('xl')]: {
            padding: theme.spacing()
        },
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        justifyItems: 'center'
    },
    linksPaper: {
        margin: theme.spacing(5)
    },
    fab: {
        zIndex: 99999,
        position: 'fixed',
        bottom: theme.spacing(3),
        right: theme.spacing(3)
    },
}))

export default function AdminPage () {
    const [links, setLinks] = useState([])
    const [dialogOpen, setDialogOpen] = React.useState(false)
    const [linkData, setLinkData] = useState({name: '', url: ''})
    const [editID, setEditID] = useState(-1)
    const [loading, setLoading] = useState(false)

    const theme = useTheme()
    const fullScreen = useMediaQuery(theme.breakpoints.down('xl'))

    const {showSnackBar} = useContext(GlobalContext)

    const classes = useStyles()

    useEffect(() => {
        getLinks()
    }, [])

    const getLinks = () => {
        LinkApi.getLinks().then(data => setLinks(data.links))
    }

    const handleDialog = () => {
        setLoading(true)
        if (editID !== -1) {
            LinkApi.editLink(editID, linkData)
                .then(() => {
                    getLinks()
                    setDialogOpen(false)
                    showSnackBar('编辑成功')
                })
                .catch(err => showSnackBar(err.toString()))
                .finally(() => setLoading(false))
        } else {
            LinkApi.addLink(linkData)
                .then(() => {
                    getLinks()
                    setDialogOpen(false)
                    showSnackBar('添加成功')
                })
                .catch(err => showSnackBar(err.toString()))
                .finally(() => setLoading(false))
        }
    }

    const handleDelete = id => () => {
        LinkApi.deleteLink(id)
            .then(() => {
                getLinks()
                showSnackBar('删除成功')
            })
            .catch(err => showSnackBar(err.toString()))
    }

    const handleEdit = link => () => {
        setEditID(link.ID)
        setLinkData({name: link.name, url: link.url})
        setDialogOpen(true)
    }

    const handleAdd = () => {
        setEditID(-1)
        setLinkData({name: '', url: ''})
        setDialogOpen(true)
    }

    return (
        <div className={classes.root}>
            <div className={classes.linksPaper}>
                {links.map(link => (
                    <Paper>
                        <Box display='flex' flexWrap='wrap' justifyContent='space-between' alignItems='baseline' p={3}
                             width='100%'>
                            <Typography>友链名称： {link.name}</Typography>
                            <Typography>友链地址： {link.url}</Typography>
                            <div>
                                <IconButton onClick={handleEdit(link)} size="large">
                                    <EditIcon/>
                                </IconButton>
                                <IconButton onClick={handleDelete(link.ID)} size="large">
                                    <DeleteIcon/>
                                </IconButton>
                            </div>
                        </Box>
                    </Paper>
                ))}
            </div>
            {!dialogOpen && <Tooltip title="添加">
                <Fab color="primary" className={classes.fab} onClick={handleAdd}>
                    <AddIcon/>
                </Fab>
            </Tooltip>}
            <Dialog
                fullScreen={fullScreen}
                open={dialogOpen}
                onClose={() => setDialogOpen(false)}
            >
                <DialogTitle>{editID === -1 ? '添加友链' : '编辑友链'}</DialogTitle>
                <DialogContent>
                    <TextField label='友链名称' fullWidth value={linkData.name} margin={"normal"}
                               onChange={e => setLinkData({...linkData, name: e.target.value})}/>
                    <TextField label='友链地址' fullWidth value={linkData.url} margin={"normal"}
                               onChange={e => setLinkData({...linkData, url: e.target.value})}/>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={() => setDialogOpen(false)} color="primary">
                        取消
                    </Button>
                    {loading ? <CircularProgress size={31}/> :
                        <Button onClick={handleDialog} color="primary">
                            {editID === -1 ? '添加' : '编辑'}
                        </Button>}
                </DialogActions>
            </Dialog>
        </div>
    );
}
