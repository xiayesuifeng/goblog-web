import { http } from '../../../api/http'

const baseUrl = '/api/links'

export const LinkApi = {
    getLinks () {
        return http.get(baseUrl)
    },

    getLink (id) {
        return http.get(baseUrl + '/' + id)
    },

    addLink (data) {
        return http.post(baseUrl, data)
    },
    editLink (id, data) {
        return http.put(baseUrl + '/' + id, data)
    },
    deleteLink (id) {
        return http.delete(baseUrl + '/' + id)
    }
}