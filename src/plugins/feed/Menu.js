import { ListItemText, MenuItem, MenuList } from '@mui/material'
import React from 'react'

export default function Menu () {
    return (
        <MenuList>
            <MenuItem component={'a'} href={'/api/feed'}>
                <ListItemText primary='订阅 (Atom)'/>
            </MenuItem>
            <MenuItem component={'a'} href={'/api/feed/rss'}>
                <ListItemText primary='订阅 (Rss)'/>
            </MenuItem>
            <MenuItem component={'a'} href={'/api/feed/json'}>
                <ListItemText primary='订阅 (Json)'/>
            </MenuItem>
        </MenuList>
    )
}
