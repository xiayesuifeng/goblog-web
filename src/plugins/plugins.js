import { PluginApi } from '../api/plugin'
import { useContext } from 'react'
import { GlobalContext } from '../globalState/Context'

async function loadPlugin (dispatch, plugins) {
    let pluginComponent = {}

    for (let i = 0; i < plugins.length; i++) {
        if (plugins[i].status !== 1)
            continue

        try {
            let manifest = await import('./' + plugins[i].packageName + '/manifest.json')
            for (let i = 0; i < manifest.component.length; i++) {
                let component = manifest.component[i]
                let tmp = await import('./' + component.url)

                if (pluginComponent[component.type] === undefined) {
                    pluginComponent[component.type] = {}
                }

                if (pluginComponent[component.type][component.position] === undefined) {
                    pluginComponent[component.type][component.position] = []
                }

                pluginComponent[component.type][component.position].push({...component, component: tmp.default})
            }
        } catch (e) {}
    }

    dispatch({type: 'SET_PLUGINS_COMPONENT', data: pluginComponent})
}

export default function initPlugins (dispatch) {
    PluginApi.getPlugin().then(data => {
        dispatch({type: 'SET_PLUGINS', data: data.plugins})

        loadPlugin(dispatch, data.plugins)
    })
}

export function usePluginComponent (type, position) {
    const {pluginComponent} = useContext(GlobalContext)

    if (pluginComponent[type] === undefined) {
        return []
    }

    if (position !== undefined || position !== '') {
        if (pluginComponent[type][position] === undefined) {
            return []
        }
    }

    return pluginComponent[type][position]
}