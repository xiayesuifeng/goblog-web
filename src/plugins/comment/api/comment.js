import { http } from '../../../api/http'

const baseUrl = '/api/comment'

export const CommentApi = {
    getComments (articleID) {
        return http.get(`${baseUrl}/${articleID}`)
    },

    addComment (data) {
        return http.post(baseUrl, data)
    },

    deleteComment (id) {
        return http.delete(`${baseUrl}/${id}`)
    }
}