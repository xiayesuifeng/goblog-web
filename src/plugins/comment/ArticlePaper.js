import React, { useContext, useEffect, useState } from 'react'
import { CommentApi } from './api/comment'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import Divider from '@mui/material/Divider'
import Typography from '@mui/material/Typography'
import TuiEditorViewer from '../../component/TuiEditorViewer'
import IconButton from '@mui/material/IconButton'
import DeleteIcon from '@mui/icons-material/Delete'
import ReplyIcon from '@mui/icons-material/Reply'
import CloseIcon from '@mui/icons-material/Close'
import Box from '@mui/material/Box'
import Cookies from 'js-cookie'
import { TextField } from '@mui/material'
import InputAdornment from '@mui/material/InputAdornment'
import AccountCircle from '@mui/icons-material/AccountCircle'
import EmailIcon from '@mui/icons-material/Email'
import Button from '@mui/material/Button'
import { GlobalContext } from '../../globalState/Context'

export default function ArticlePaper (props) {
    const {articleID} = props
    const [comments, setComments] = useState([])
    const [login, setLogin] = useState(false)
    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [comment, setComment] = useState('')
    const [replyID, setReplyID] = useState(-1)

    const {showSnackBar} = useContext(GlobalContext)

    useEffect(() => {
        const login = Cookies.get('goblog-session') !== undefined
        setLogin(login)
        if (login)
            setUsername('Author')

        if (articleID !== undefined && articleID !== '') {
            getComments()
        }
    }, [articleID])

    const getComments = () => {
        CommentApi.getComments(articleID).then(data => setComments(data.comments))
    }

    const formatTime = time => {
        time = time.replace('T', ' ')
        time = time.replace('Z', '')
        return new Date(time).toLocaleDateString()
    }

    const handleAdd = () => {
        if (username === '') {
            showSnackBar('用户名不能为空')
            return
        }

        if (comment === '') {
            showSnackBar('评论不能为空')
            return
        }

        if (!login && (username === 'Author' || username === '作者')) {
            showSnackBar('用户名不得为 Author')
            return
        }

        CommentApi.addComment({
            articleID: parseInt(articleID),
            username: username,
            email: email,
            comment: comment,
            parentID: replyID !== -1 ? replyID : undefined
        })
            .then(() => {
                getComments()
                setComment('')
                setReplyID(-1)
                scrollToAnchor(replyID !== -1 ? replyID.toString() : 'comment')
                showSnackBar('提交成功')
            })
            .catch(err => showSnackBar(err.toString()))
    }

    const handleReply = id => () => {
        setReplyID(id)
        scrollToAnchor('addComment')
    }

    const handleDelete = id => () => {
        CommentApi.deleteComment(id)
            .then(() => {
                getComments()
                showSnackBar('删除成功')
            })
            .catch(err => showSnackBar(err.toString()))
    }

    const scrollToAnchor = anchorName => {
        if (anchorName) {
            let anchorElement = document.getElementById(anchorName)
            if (anchorElement) {
                let bodyTop = document.body.getBoundingClientRect().top
                window.scrollTo({
                    top: anchorElement.getBoundingClientRect().top - bodyTop,
                    behavior: 'smooth'
                })
            }
        }
    }

    const CommentListItem = (comment, isSub) => {
        return (
            <Box pl={5} key={comment.ID} pr={!isSub ? 5 : 0}>
                <ListItem id={comment.ID}>
                    <Box flex={'1 1 auto'} mt={'6px'} mb={'6px'} minWidth={'0'}>
                        <Typography
                            variant={'body1'}>{comment.username === 'Author' ? '作者' : comment.username}</Typography>
                        <TuiEditorViewer value={comment.comment}/>
                    </Box>
                    <Box display='flex' flexDirection='column' alignItems='flex-end'>
                        <Typography variant={'body2'}>发布于 {formatTime(comment.CreatedAt)}</Typography>
                        <Box>
                            {login && <IconButton onClick={handleDelete(comment.ID)} size="large">
                                <DeleteIcon/>
                            </IconButton>}
                            <IconButton onClick={handleReply(comment.ID)} size="large">
                                <ReplyIcon/>
                            </IconButton>
                        </Box>
                    </Box>
                </ListItem>
                <Divider/>
                {comment.subComment.map(subC => CommentListItem(subC, true))}
            </Box>
        );
    }

    return (
        <div id='comment'>
            <Box p={3}>
                <Typography variant={'h6'}>评论</Typography>
                <List>
                    {comments.map(comment => CommentListItem(comment, false))}
                </List>
            </Box>
            <Box bgcolor="#EEE" p={3} id='addComment'>
                <Typography variant={'h6'}>添加评论</Typography>

                <Box display='flex' flexWrap='wrap' p={3}>
                    <Box flexGrow={1} pr={3}>
                        <TextField
                            fullWidth
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <AccountCircle/>
                                    </InputAdornment>
                                ),
                            }}
                            onChange={e => setUsername(e.target.value)}
                            value={username} label={'用户名'}/>
                    </Box>
                    <Box flexGrow={2}>
                        <TextField
                            fullWidth
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <EmailIcon/>
                                    </InputAdornment>
                                ),
                            }}
                            type={'email'}
                            onChange={e => setEmail(e.target.value)}
                            value={email} label={'邮箱'}/>
                    </Box>
                    {replyID !== -1 &&
                    <Box display='flex' flexGrow={1} alignItems='center' pl={3}>
                        <Typography variant={'body1'}>回复自 #{replyID}</Typography>
                        <IconButton size={'small'} onClick={() => setReplyID(-1)}>
                            <CloseIcon/>
                        </IconButton>
                    </Box>}
                </Box>
                <Box p={3} pt={0}>
                    <TextField
                        multiline
                        fullWidth
                        rows='5'
                        onChange={e => setComment(e.target.value)}
                        value={comment} label={'评论内容 (支持 Markdown 语法)'}/>
                </Box>
                <Box pr={3} display='flex' flexDirection='column' alignItems='flex-end'>
                    <Button onClick={handleAdd} color={'primary'} variant={'contained'}>提交</Button>
                </Box>
            </Box>
        </div>
    )
}