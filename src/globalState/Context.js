import React from 'react'

export const initGlobalValue = {
    info: {
        title: 'goblog',
        useCategory: true,
        logo: 'none'
    },
    categories: [],
    plugins: [],
    pluginComponent: {}
}

export const GlobalContext = React.createContext(initGlobalValue)