export function GlobalReducer(state, action) {
    switch(action.type) {
        case 'SET_INFO':
            return {...state,info:action.data}
        case 'SET_CATEGORIES':
            return {...state,categories:action.data}
        case 'SET_PLUGINS':
            return {...state,plugins:action.data}
        case 'SET_PLUGINS_COMPONENT':
            return {...state,pluginComponent:action.data}
        default:
            return state
    }
}