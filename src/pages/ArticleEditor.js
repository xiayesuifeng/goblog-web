import React, { useContext, useEffect, useState } from 'react'
import { AppBar, Hidden, IconButton, MenuItem, Paper, TextField, Toolbar, Tooltip, Typography } from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import DoneIcon from '@mui/icons-material/Done'
import { AdminApi } from '../api/admin'
import { ArticleApi } from '../api/article'
import Fab from '@mui/material/Fab'
import { useTheme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { GlobalContext } from '../globalState/Context'
import useMediaQuery from '@mui/material/useMediaQuery'
import {Link, useNavigate, useParams} from 'react-router-dom'
import Button from '@mui/material/Button'
import Box from '@mui/material/Box'
import 'codemirror/lib/codemirror.css'
import '@toast-ui/editor/dist/toastui-editor.css'
import 'tui-color-picker/dist/tui-color-picker.css';
import 'highlight.js/styles/github.css'
import hljs from 'highlight.js'
import codeSyntaxHighlight from '@toast-ui/editor-plugin-code-syntax-highlight'
import Editor from '@toast-ui/editor'
import '@toast-ui/editor/dist/i18n/zh-cn'
import table from '@toast-ui/editor-plugin-table-merged-cell'
import colorSyntax from '@toast-ui/editor-plugin-color-syntax'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'
import Autocomplete from '@mui/material/Autocomplete';
import axios from "axios";

const useStyles = makeStyles(theme => ({
    toolbar: theme.mixins.toolbar,
    root: {
        display: 'flex',
        textAlign: 'left',
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        height: '100vh'
    },
    info: {
        width: '100%',
        display: 'inline-flex',
        flexWrap: 'wrap',
        marginBottom: theme.spacing()
    },
    editor: {
        display: 'inline-flex',
        flexDirection: 'column',
        padding: theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            width: '80%',
            height: '80%'
        },
        [theme.breakpoints.down('xl')]: {
            flex: 1,
        }
    },
    ArrowBackButton: {
        zIndex: 99999,
        position: 'fixed',
        top: theme.spacing(),
        left: theme.spacing()
    },
    doneFAB: {
        zIndex: theme.zIndex.speedDial,
        position: 'fixed',
        bottom: theme.spacing(3),
        right: theme.spacing(3)
    },
    nameField: {
        flex: 1,
        marginLeft: theme.spacing()
    },
    categoryField: {
        minWidth: 150
    },
    tagField: {
        width: 150,
        marginLeft: theme.spacing()
    },
    editorDiv: {
        flex: 1
    }
}))

export default function ArticleEditor (props) {
    const [edit, setEdit] = useState(false)
    const [editor, setEditor] = useState(null)
    const [markdown, setMarkdown] = useState('')
    const [category, setCategory] = useState(0)
    const [tag, setTag] = useState('')
    const [tags, setTags] = useState([])
    const [title, setTitle] = useState('')
    const [privateArticle, setPrivateArticle] = useState(false)

    const {id} = useParams()

    const {info, categories, showSnackBar} = useContext(GlobalContext)

    const navigate = useNavigate()

    const classes = useStyles()

    const theme = useTheme()
    const matches = useMediaQuery(theme.breakpoints.down('xl'))

    useEffect(() => {
        let editor = new Editor({
            el: document.querySelector('#tui-editor'),
            language: 'zh-CN',
            height: '100%',
            plugins: [table, colorSyntax,[codeSyntaxHighlight,{ hljs }]],
        })

        editor.addHook('addImageBlobHook', function (blob, callback) {
            AdminApi.putAssets(blob)
                .then(data => callback(data.path, 'alt text'))
                .catch(err => showSnackBar('上传失败!' + err))
        })

        setEditor(editor)
    }, [matches])

    useEffect(() => {
        if (editor !== null)
            editor.changePreviewStyle(matches ? 'tab' : 'vertical')
    }, [matches, editor])

    useEffect(() => {
        if (id !== undefined) {
            ArticleApi.getArticleById(id)
                .then(data => {
                    document.title = '编辑 ' + data.article.title

                    ArticleApi.getArticleMDByUUID(data.article.Uuid)
                        .then(data => {
                            setMarkdown(data.markdown)
                        })
                    setTitle(data.article.title)
                    setCategory(data.article.category_id)
                    setTag(data.article.tag)
                    setPrivateArticle(data.article.private)
                    setEdit(true)
                })
        } else {
            document.title = '新建文章'
        }

        axios.get('/api/tag')
            .then(r => {
                if (r.data.code === 200)
                    setTags(r.data.tags)
            })
    }, [id])

    useEffect(() => {
        if (editor) {
            editor.setMarkdown(markdown)
        }
    }, [markdown, editor])

    const handleSubmit = useCategory => () => {
        let data = {
            title: title,
            tag: tag,
            context: editor.getMarkdown(),
            private: privateArticle
        }

        if (useCategory)
            data = {...data, category_id: category}

        if (edit) {
            ArticleApi.editArticle(id, data)
                .then(() => navigate('/article/' + id))
                .catch(err => showSnackBar('提交失败!' + err))
        } else {
            ArticleApi.addArticle(data)
                .then(() => navigate('/'))
                .catch(err => showSnackBar('提交失败!' + err))
        }
    }

    return (
        <div className={classes.root}>
            <Hidden mdUp>
                <AppBar>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            component={Link}
                            to="/"
                            size="large">
                            <ArrowBackIcon/>
                        </IconButton>
                        <Typography variant="title" color="inherit">{document.title}</Typography>
                    </Toolbar>
                </AppBar>
                <div className={classes.toolbar}/>
            </Hidden>
            <Hidden xlDown>
                <Tooltip id="tooltip-fab" title="返回">
                    <IconButton
                        aria-label="ArrowBack"
                        className={classes.ArrowBackButton}
                        onClick={() => navigate("..")}
                        size="large">
                        <ArrowBackIcon/>
                    </IconButton>
                </Tooltip>
            </Hidden>
            <Paper className={classes.editor}>
                <div className={classes.info}>
                    {info.useCategory &&
                    <TextField
                        select
                        label="分类"
                        fullWidth={matches}
                        value={category}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        className={classes.categoryField}
                        size={"small"}
                        onChange={e => setCategory(parseInt(e.target.value))}
                    >
                        {categories.map(category => (
                            <MenuItem key={category.ID} value={category.ID}>
                                {category.name}
                            </MenuItem>
                        ))}
                    </TextField>}
                    <Autocomplete
                        freeSolo
                        fullWidth={matches}
                        size={"small"}
                        options={tags}
                        className={classes.tagField}
                        value={tag}
                        renderInput={(params) =>
                            <TextField {...params} label="标签" onChange={e => setTag(e.target.value)}/>}
                    />
                    <TextField
                        id="name"
                        label="标题"
                        fullWidth={matches}
                        value={title}
                        size={"small"}
                        className={classes.nameField}
                        onChange={e => setTitle(e.target.value)}
                    />
                </div>
                <div className={classes.editorDiv}>
                    <div id="tui-editor"/>
                </div>
                <Box pt={1}>
                    <FormControlLabel
                        control={
                            <Switch
                                checked={privateArticle}
                                onChange={e => setPrivateArticle(e.target.checked)}
                                color="primary"
                            />
                        }
                        label="私有文章"
                    />
                </Box>
                <Hidden mdUp>
                    <Box pt={2}>
                        <Button fullWidth color="primary" variant={'contained'} onClick={handleSubmit(info.useCategory)}>提交</Button>
                    </Box>
                </Hidden>
            </Paper>
            <Hidden xlDown>
                <Tooltip title="提交">
                    <Fab color="primary" className={classes.doneFAB}
                         onClick={handleSubmit(info.useCategory)}>
                        <DoneIcon/>
                    </Fab>
                </Tooltip>
            </Hidden>
        </div>
    );
}
