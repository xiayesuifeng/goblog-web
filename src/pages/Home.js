import React, { useContext, useEffect, useRef, useState } from 'react'
import logo from '../logo.svg'
import MenuIcon from '@mui/icons-material/Menu'
import AddIcon from '@mui/icons-material/Add'
import {
    AppBar,
    Avatar,
    Divider,
    Drawer,
    Hidden,
    IconButton,
    ListItemText,
    MenuItem,
    MenuList,
    Toolbar,
    Typography,
} from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import axios from 'axios'
import {Link, useParams,useNavigate} from 'react-router-dom'
import Cookies from 'js-cookie'
import SpeedDial from '@mui/material/SpeedDial'
import SpeedDialIcon from '@mui/material/SpeedDialIcon'
import SpeedDialAction from '@mui/material/SpeedDialAction'
import CategoryIcon from '@mui/icons-material/Category'
import SettingsIcon from '@mui/icons-material/Settings'
import ExitToAppIcon from '@mui/icons-material/ExitToApp'
import { AdminApi } from '../api/admin'
import LinearProgress from '@mui/material/LinearProgress'
import ArticleCard from '../component/ArticleCard'
import useMediaQuery from '@mui/material/useMediaQuery'
import { GlobalContext } from '../globalState/Context'
import Box from '@mui/material/Box'
import { usePluginComponent } from '../plugins/plugins'
import CategoryDialog from '../component/CategoryDialog'
import CategoryMenuList from '../component/CatogoryMenuList'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: 250,
        [theme.breakpoints.up('md')]: {
            position: 'relative',
        },
    },
    header: {
        padding: theme.spacing(3),
        height: 249,
        width: 249,
    },
    logo: {
        margin: 'auto',
        marginBottom: theme.spacing(3),
        height: 150,
        width: 150,
    },
    content: {
        flexGrow: 1,
        height: '100vh',
        backgroundColor: theme.palette.background.default,
        overflow: 'auto'
    },
    addFAB: {
        zIndex: theme.zIndex.speedDial,
        position: 'fixed',
        bottom: theme.spacing(3),
        right: theme.spacing(3)
    },
}))

function Home (props) {
    const [mobileOpen, setMobileOpen] = useState(false)
    const [dialogOpen, setDialogOpen] = useState(false)
    const [speedDialOpen, setSpeedDialOpen] = useState(false)
    const [tags, setTags] = useState([])
    const [articles, setArticles] = useState([])
    const [category, setCategory] = useState(0)
    const [tag, setTag] = useState('')
    const [login, setLogin] = useState(false)
    const [loading, setLoading] = useState(true)
    const [articlesLoading, setArticlesLoading] = useState(true)

    const navigate = useNavigate()

    const {type,id} = useParams()

    const classes = useStyles()

    const {info, categories, showSnackBar} = useContext(GlobalContext)

    const mainRef = useRef()

    const mdUP = useMediaQuery(theme => theme.breakpoints.up('md'))

    const menus = usePluginComponent('menu', 'bottom')

    useEffect(() => {
        document.title = info.title
    }, [info.title])

    useEffect(() => {
        setLogin(Cookies.get('goblog-session') !== undefined)

        axios.get('/api/tag')
            .then(r => {
                if (r.data.code === 200)
                    setTags(r.data.tags)
            })
    }, [])

    useEffect(() => {
        let url = '/api/article'
        if (type !== undefined) {
            if (type === 'category') {
                url = `${url}/${type}/${id}`
                setCategory(parseInt(id))
                setTag('')
            } else if (type === 'tag') {
                url = `/api/${type}/${id}`
                setCategory(0)
                setTag(id)
            }
        } else {
            setCategory(0)
            setTag('')
        }

        getArticles(url)
    }, [type,id])

    function getArticles (url) {
        setLoading(true)
        axios.get(url).then(r => {
            if (r.data.code === 200) {
                setArticles(r.data.articles)
                setLoading(false)
                if (articlesLoading)
                    setArticlesLoading(false)
            }
        })
    }

    const handleClick = () => setSpeedDialOpen(!speedDialOpen)

    const handleOpen = () => setSpeedDialOpen(login)

    const handleClose = () => setSpeedDialOpen(false)

    const handleLogout = () => {
        AdminApi.logout()
            .then(() => {
                Cookies.remove('goblog-session')
                setLogin(false)
                showSnackBar('退出成功')
            })
            .catch(err => showSnackBar(err.toString()))
    }

    const drawer = (
        <Box display='flex' flexDirection='column' justifyContent='space-between' height='100%'>
            <div>
                <div className={classes.header}>
                    <Avatar src={info.logo === 'none' ? logo : info.logo} alt="logo"
                            className={classes.logo}/>
                    <Typography variant="h6">{info.title}</Typography>
                </div>
                {info.useCategory && <CategoryMenuList category={category} />}
                <Divider/>
                <MenuList>
                    <MenuItem component={Link} to='/'>
                        <ListItemText primary="全部标签"/>
                    </MenuItem>
                    {tags.map((t) => {
                        return (
                            <MenuItem selected={tag === t} component={Link} to={'/tag/' + t}>
                                <ListItemText primary={t}/>
                            </MenuItem>
                        )
                    })}
                </MenuList>
            </div>
            <div>
                {menus.map(Menu => (
                    <div>
                        <Divider/>
                        <Menu.component/>
                    </div>
                ))}
            </div>
        </Box>
    )

    return (
        <div className={classes.root}>
            {info.useCategory && <CategoryDialog open={dialogOpen} onClose={() => setDialogOpen(false)}/>}
            <Hidden mdUp>
                <AppBar>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={() => setMobileOpen(!mobileOpen)}
                            size="large">
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="h6" color="inherit">{info.title}</Typography>

                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="temporary"
                    open={mobileOpen}
                    onClose={() => setMobileOpen(!mobileOpen)}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    ModalProps={{
                        keepMounted: true,
                    }}
                >
                    {drawer}
                </Drawer>
            </Hidden>
            <Hidden xlDown>
                <Drawer
                    variant="permanent"
                    open
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    {drawer}
                </Drawer>
            </Hidden>
            <main className={classes.content} ref={mainRef}>
                <Hidden mdUp>
                    <div className={classes.toolbar}/>
                </Hidden>
                <LinearProgress sx={{display: loading?'block':'none'}} variant="query"/>
                {(articlesLoading ?
                    Array.from(new Array(mainRef.current ?
                        parseInt(mainRef.current.clientHeight / (mdUP ? 300 : 240)) : 3)) : articles)
                    .map((article) => (
                        article ? <ArticleCard article={article} categories={categories}/>
                            : <ArticleCard loading={articlesLoading}/>
                    ))}
                <SpeedDial
                    ariaLabel="SpeedDial"
                    className={classes.addFAB}
                    hidden={!login}
                    icon={<SpeedDialIcon/>}
                    onBlur={handleClose}
                    onClick={handleClick}
                    onClose={handleClose}
                    onFocus={handleOpen}
                    onMouseEnter={handleOpen}
                    onMouseLeave={handleClose}
                    open={speedDialOpen}
                >
                    <SpeedDialAction
                        icon={<ExitToAppIcon/>}
                        tooltipTitle="退出登录"
                        onClick={handleLogout}
                    />
                    <SpeedDialAction
                        icon={<SettingsIcon/>}
                        tooltipTitle="后台管理"
                        onClick={() => navigate('/admin')}
                    />
                    {info.useCategory &&
                    <SpeedDialAction
                        icon={<CategoryIcon/>}
                        tooltipTitle="分类"
                        onClick={() => setDialogOpen(true)}
                    />}
                    <SpeedDialAction
                        icon={<AddIcon/>}
                        tooltipTitle="添加"
                        onClick={() => navigate('/articleEditor')}
                    />
                </SpeedDial>
            </main>
        </div>
    );
}

export default Home
