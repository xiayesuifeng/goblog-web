import React, { useContext, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { Button, Paper, TextField } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { AdminApi } from '../api/admin'
import CircularProgress from '@mui/material/CircularProgress'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import { GlobalContext } from '../globalState/Context'

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        height: '100vh'
    },
    login: {
        padding: theme.spacing(3),
        minWidth: 350,
    },
    header: {
        padding: '20px 0',
        marginTop: -60,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 15,
        background: 'linear-gradient(60deg, #6ec6ff, #0069c0)',
        color: 'white',
        '& > h5': {
            padding: theme.spacing(2)
        }
    },
}))

export default function Login (props) {
    const classes = useStyles()

    const [password, setPassword] = useState('')
    const [message, setMessage] = useState('')
    const [loading, setLoading] = useState(false)

    const {showSnackBar} = useContext(GlobalContext)

    const navigate = useNavigate()

    const handleLogin = () => {
        if (password === '')
            return

        setLoading(true)

        AdminApi.login(password)
            .then(() => {
                setMessage('')
                showSnackBar('登录成功')
                navigate('/')
            })
            .catch(err => {
                if (err.toString() === 'Error: password errors') {
                    setMessage('密码错误')
                    setLoading(false)
                } else {
                    setMessage(err.toString())
                    setLoading(false)
                }
            })
    }

    useEffect(() => {
        document.title = '登陆'
    }, [])

    return (
        <div className={classes.root}>
            <Paper className={classes.login}>
                <Paper className={classes.header} elevation='15'>
                    <Typography variant="h5">
                        登录
                    </Typography>
                </Paper>
                <TextField
                    error={message !== ''}
                    id="password"
                    label={message !== '' ? message : '密码'}
                    type="password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    margin="normal"
                    fullWidth
                />
                <Box pt={2}>
                    {loading ? <CircularProgress size={31}/> :
                        <Button variant="contained" color="primary" onClick={handleLogin}>登录</Button>}
                </Box>
            </Paper>
        </div>
    )
}
