import React, { useContext, useEffect, useState } from 'react'
import { AppBar, Avatar, Hidden, IconButton, Paper, Toolbar, Tooltip, Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import SpeedDial from '@mui/material/SpeedDial'
import SpeedDialIcon from '@mui/material/SpeedDialIcon'
import SpeedDialAction from '@mui/material/SpeedDialAction'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'
import {Link, useNavigate, useParams} from 'react-router-dom'
import Cookies from 'js-cookie'
import TuiEditorViewer from '../component/TuiEditorViewer'
import { ArticleApi } from '../api/article'
import { CategoryApi } from '../api/category'
import LinearProgress from '@mui/material/LinearProgress'
import Skeleton from '@mui/material/Skeleton'
import Box from '@mui/material/Box'
import { GlobalContext } from '../globalState/Context'
import { usePluginComponent } from '../plugins/plugins'

const useStyles = makeStyles(theme => ({
    toolbar: theme.mixins.toolbar,
    root: {
        textAlign: 'left',
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        minHeight: `calc(100vh - 4px)`,
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(10),
            boxSizing: 'border-box'
        },
    },
    avatar: {
        backgroundColor: theme.palette.secondary.main
    },
    context: {
        paddingLeft: theme.spacing(20),
        paddingRight: theme.spacing(20),
        paddingBottom: theme.spacing(20),
        paddingTop: theme.spacing(2),
        [theme.breakpoints.down('xl')]: {
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(5),
            paddingLeft: theme.spacing(5),
            paddingRight: theme.spacing(5)
        },
    },
    article: {
        display: 'inline-flex',
        flexDirection: 'column',
        [theme.breakpoints.up('md')]: {
            minWidth: 800,
        },
        [theme.breakpoints.down('xl')]: {
            flex: 1,
            width: '100%'
        },
    },
    pluginTopPaper: {
        marginBottom: theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            minWidth: 800,
        },
        [theme.breakpoints.down('xl')]: {
            width: '100%'
        },
    },
    pluginBottomPaper: {
        marginTop: theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            minWidth: 800,
        },
        [theme.breakpoints.down('xl')]: {
            width: '100%'
        },
    },
    header: {
        padding: theme.spacing(3),
        display: 'inline-flex',
        alignItems: 'center'
    },
    headerInfo: {
        paddingLeft: theme.spacing(),
        display: 'inline-flex',
        flexWrap: 'wrap',
        flex: 1,
        justifyContent: 'space-between',
    },
    ArrowBackButton: {
        zIndex: 99999,
        position: 'fixed',
        top: theme.spacing(),
        left: theme.spacing()
    },
    speedDial: {
        zIndex: theme.zIndex.speedDial,
        position: 'fixed',
        bottom: theme.spacing(3),
        right: theme.spacing(3)
    }
}))

function Article (props) {
    const [article, setArticle] = useState({})
    const [category, setCategory] = useState('')
    const [markdown, setMarkdown] = useState('')
    const [time, setTime] = useState('')
    const [open, setOpen] = useState(false)
    const [login, setLogin] = useState(false)
    const [loading, setLoading] = useState(true)

    const {id:articleID} = useParams()

    const navigate = useNavigate()

    const {info, showSnackBar} = useContext(GlobalContext)

    const classes = useStyles()

    let isTouch
    if (typeof document !== 'undefined') {
        isTouch = 'ontouchstart' in document.documentElement
    }

    useEffect(() => {
        setLogin((Cookies.get('goblog-session') !== undefined))
        ArticleApi.getArticleById(articleID)
            .then(data => {
                document.title = data.article.title

                ArticleApi.getArticleMDByUUID(data.article.Uuid)
                    .then(data => {
                        setMarkdown(data.markdown)
                        setLoading(false)
                    })
                CategoryApi.getCategoryName(data.article.category_id)
                    .then(data => {
                        setCategory(data.category.name)
                    })

                if (data.article.CreatedAt === data.article.UpdatedAt) {
                    setTime(formatTime(data.article.CreatedAt))
                } else {
                    setTime(formatTime(data.article.UpdatedAt))
                }

                setArticle(data.article)
            })
    }, [articleID])

    const articleTopPapers = usePluginComponent('articlePaper', 'top')
    const articleBottomPapers = usePluginComponent('articlePaper', 'bottom')

    const formatTime = time => {
        time = time.replace('T', ' ')
        time = time.replace('Z', '')
        return new Date(time).toLocaleDateString()
    }

    const handleOpen = () => {
        if (login)
            setOpen(true)
    }

    const handleDelete = () => {
        setLoading(true)
        ArticleApi.deleteArticle(articleID)
            .then(() => navigate('/'))
            .catch(err => {
                showSnackBar('删除失败!错误:' + err)
                setLoading(false)
            })
    }

    return (
        <div>
            <Hidden mdUp>
                <AppBar>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            component={Link}
                            to="/"
                            size="large">
                            <ArrowBackIcon/>
                        </IconButton>
                        <Typography variant="title" color="inherit">{article.title}</Typography>
                    </Toolbar>
                </AppBar>
            </Hidden>
            <Hidden xlDown>
                <Tooltip id="tooltip-fab" title="返回首页">
                    <IconButton
                        aria-label="ArrowBack"
                        className={classes.ArrowBackButton}
                        component={Link}
                        to="/"
                        size="large">
                        <ArrowBackIcon/>
                    </IconButton>
                </Tooltip>
            </Hidden>
            <Hidden mdUp>
                <div className={classes.toolbar}/>
            </Hidden>
            <LinearProgress sx={{display: loading?'block':'none'}} variant="query"/>
            <div className={classes.root}>
                {articleTopPapers.map(ArticlePaper => (
                    <Paper className={classes.pluginTopPaper}>
                        <ArticlePaper.component articleID={articleID}/>
                    </Paper>
                ))}
                <Paper className={classes.article}>
                    <div className={classes.header}>
                        {loading ? <Skeleton variant="circular" width={40} height={40}/>
                            : <Avatar aria-label="Recipe" className={classes.avatar}>
                                {article.tag}
                            </Avatar>}
                        <div className={classes.headerInfo}>
                            <Hidden xlDown>
                                {loading ? <Skeleton width={200}/> : <span>{article.title}</span>}
                            </Hidden>
                            {info.useCategory && !loading ?
                                <span>归类于 {category ? category : '已删除的分类'}</span> : <span/>}
                            {loading ? <Skeleton width={150}/> : <span>更新于 {time}</span>}
                        </div>
                    </div>
                    <div className={classes.context}>
                        {loading ? <Box>
                                <Skeleton/>
                                <Skeleton/>
                                <Skeleton/>
                                <Skeleton width="60%"/>
                            </Box>
                            : <TuiEditorViewer value={markdown}/>}
                    </div>
                </Paper>
                {articleBottomPapers.map(ArticlePaper => (
                    <Paper className={classes.pluginBottomPaper}>
                        <ArticlePaper.component articleID={articleID}/>
                    </Paper>
                ))}
            </div>
            <SpeedDial
                ariaLabel="SpeedDial"
                className={classes.speedDial}
                hidden={!login}
                icon={<SpeedDialIcon/>}
                onBlur={() => setOpen(false)}
                onClick={() => setOpen(!open)}
                onClose={() => setOpen(false)}
                onFocus={isTouch ? undefined : handleOpen}
                onMouseEnter={isTouch ? undefined : handleOpen}
                onMouseLeave={() => setOpen(false)}
                open={open}
            >
                <SpeedDialAction
                    icon={<DeleteIcon/>}
                    tooltipTitle="删除"
                    onClick={handleDelete}
                />
                <SpeedDialAction
                    icon={<EditIcon/>}
                    tooltipTitle="编辑"
                    onClick={() => navigate(`/articleEditor/${articleID}`)}
                />
            </SpeedDial>
        </div>
    );
}

export default Article
