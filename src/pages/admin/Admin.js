import React, { useContext, useEffect, useState } from 'react'
import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import ListItemText from '@mui/material/ListItemText'
import Drawer from '@mui/material/Drawer'
import Hidden from '@mui/material/Hidden'
import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu'
import {Link, Route, Routes, useLocation, useNavigate} from 'react-router-dom'
import Cookies from 'js-cookie'
import MenuList from '@mui/material/MenuList'
import MenuItem from '@mui/material/MenuItem'
import Info from './Info'
import Article from './Article'
import Category from './Category'
import { GlobalContext } from '../../globalState/Context'
import makeStyles from '@mui/styles/makeStyles';
import PluginCenter from './PluginCenter'
import Button from '@mui/material/Button'
import { usePluginComponent } from '../../plugins/plugins'

const drawerWidth = 240

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flex: 1,
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    title: {
        textAlign: 'left',
        flexGrow: 1,
    },
}))

export default function Admin (props) {
    const [mobileOpen, setMobileOpen] = useState(false)

    const navigate = useNavigate()
    const location = useLocation()

    const {info} = useContext(GlobalContext)

    const adminPages = usePluginComponent('adminPage')

    const classes = useStyles()

    useEffect(() => {
        let login = Cookies.get('goblog-session') !== undefined
        if (!login)
            navigate('/login')

        if (location.pathname === '/admin')
            navigate('/admin/info')
    }, [location])

    const drawer = (
        <div>
            <div className={classes.toolbar}/>
            <Divider/>
            <MenuList>
                <MenuItem selected={location.pathname === '/admin/info'} component={Link} to={'/admin/info'}>
                    <ListItemText primary="博客信息"/>
                </MenuItem>
                <MenuItem selected={location.pathname === '/admin/article'} component={Link}
                          to={'/admin/article'}>
                    <ListItemText primary="文章"/>
                </MenuItem>
                {info.useCategory &&
                <MenuItem selected={location.pathname === '/admin/category'} component={Link}
                          to={'/admin/category'}>
                    <ListItemText primary="分类"/>
                </MenuItem>}
                <MenuItem selected={location.pathname === '/admin/pluginCenter'} component={Link}
                          to={'/admin/pluginCenter'}>
                    <ListItemText primary="插件中心"/>
                </MenuItem>
                {adminPages.map(page => (
                    <MenuItem selected={location.pathname === page.uri} component={Link}
                              to={`/admin${page.uri}`}>
                        <ListItemText primary={page.name}/>
                    </MenuItem>
                ))}
            </MenuList>
        </div>
    )

    return (
        <div className={classes.root}>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="Open drawer"
                        onClick={() => setMobileOpen(!mobileOpen)}
                        className={classes.menuButton}
                        size="large">
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" color="inherit" noWrap className={classes.title}>
                        后台管理
                    </Typography>
                    <Button color="inherit" variant='outlined' onClick={() => navigate('/')}>退出后台</Button>
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer}>
                <Hidden smUp implementation="css">
                    <Drawer
                        container={props.container}
                        variant="temporary"
                        open={mobileOpen}
                        onClose={() => setMobileOpen(!mobileOpen)}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true,
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xlDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className={classes.content}>
                <div className={classes.toolbar}/>
                <Routes>
                    <Route path="/info" element={<Info />}/>
                    <Route path="/article" element={<Article />}/>
                    <Route path="/category" element={<Category />}/>
                    <Route path="/pluginCenter" element={<PluginCenter />}/>
                    {adminPages.map(page => (
                        <Route path={page.uri} element={<page.component />}/>
                    ))}
                </Routes>
            </main>
        </div>
    );
}
