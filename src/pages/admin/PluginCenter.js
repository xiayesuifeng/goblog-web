import React, { useContext, useEffect, useState } from 'react'
import { Tooltip } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Fab from '@mui/material/Fab'
import RefreshIcon from '@mui/icons-material/Refresh'
import Paper from '@mui/material/Paper'
import { PluginApi } from '../../api/plugin'
import Chip from '@mui/material/Chip'
import { GlobalContext } from '../../globalState/Context'
import initPlugins from '../../plugins/plugins'
import IconButton from '@mui/material/IconButton'
import DeleteIcon from '@mui/icons-material/Delete'
import CloudDownloadIcon from '@mui/icons-material/CloudDownload'
import SystemUpdateAltIcon from '@mui/icons-material/SystemUpdateAlt'
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import CircularProgress from '@mui/material/CircularProgress'
import DialogContentText from '@mui/material/DialogContentText'
import Backdrop from '@mui/material/Backdrop'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3),
        [theme.breakpoints.down('xl')]: {
            padding: theme.spacing()
        },
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    plugin: {
        display: 'flex',
        flexDirection: 'column',
        padding: theme.spacing(1, 1, 5, 5),
        height: 140,
        width: 300,
        margin: theme.spacing(3)
    },
    fab: {
        zIndex: theme.zIndex.speedDial,
        position: 'fixed',
        bottom: theme.spacing(3),
        right: theme.spacing(3)
    },
    backdrop: {
        zIndex: theme.zIndex.drawer - 10,
        [theme.breakpoints.up('sm')]: {
            marginLeft: 240
        },
        color: '#fff',
    }
}))

function DownloadDialog (props) {
    return (<Dialog open={props.open}>
        <DialogContent>
            <CircularProgress/>
            <DialogContentText>
                {props.message}
            </DialogContentText>
        </DialogContent>
    </Dialog>)
}

function PluginPaper (props) {
    const [status, setStatus] = useState('')
    const [dialogOpen, setDialogOpen] = useState(false)
    const [dialogMsg, setDialogMsg] = useState('')

    const classes = useStyles()

    const {plugin} = props

    const {plugins, showSnackBar} = useContext(GlobalContext)

    useEffect(() => {
        setStatus(getPluginStatus(plugin.packageName, plugin.pluginVersion))
    }, [plugin, plugins])

    const statusConfig = {
        '已安装': {
            icon: (<DeleteIcon fontSize="small"/>),
            color: 'primary',
        },
        '版本不兼容': {
            icon: (<RefreshIcon fontSize="small"/>),
            color: 'secondary',
        },
        '可更新': {
            icon: (<SystemUpdateAltIcon fontSize="small"/>),
            color: 'secondary',
        },
        '未安装': {
            icon: (<CloudDownloadIcon fontSize="small"/>),
            color: 'default',
        },
        'default': {
            icon: (<CloudDownloadIcon fontSize="small"/>),
            color: 'default',
        }
    }

    const checkPluginVersion = (ver1, ver2) => {
        const ver1s = ver1.split('.')
        const ver2s = ver2.split('.')

        for (let i = 0; i < 3; i++) {
            if (ver1s[i] > ver2s[i])
                return true
        }

        return false
    }

    const getPluginStatus = (name, version) => {
        for (let i = 0; i < plugins.length; i++) {
            if (plugins[i].packageName === name) {
                return plugins[i].status === 2 ? '版本不兼容' :
                    checkPluginVersion(version, plugins[i].pluginVersion) ? '可更新' : '已安装'
            }
        }

        return '未安装'
    }

    const showCountdownDialog = () => {
        let sec = 5
        setDialogMsg(`将在${sec}秒后刷新插件状态`)

        const timer = setInterval(function () {
            setDialogMsg(`将在${--sec}秒后刷新插件状态`)
            if (sec === 0) {
                clearInterval(timer)
                setDialogOpen(false)
                props.refreshPlugins()
            }
        }, 1000)
    }

    const handlePlugin = () => {
        if (status === '已安装') {
            handelDelete()
        } else {
            setDialogMsg('请稍等')
            setDialogOpen(true)
            const protocol = window.location.protocol === 'https:' ? 'wss:' : 'ws:'

            let ws = new WebSocket(`${protocol}//${window.location.host}/api/plugin/${plugin.packageName}`)

            let msg = {}
            ws.onmessage = function (e) {
                msg = JSON.parse(e.data)
                setDialogMsg(msg.status)
            }

            ws.onclose = function () {
                if (msg.status === '下载完成') {
                    showCountdownDialog()
                } else {
                    showSnackBar(dialogMsg)
                    setDialogOpen(false)
                }
            }
        }
    }

    const handelDelete = () => {
        PluginApi.deletePlugin(plugin.packageName)
            .then(() => {
                setDialogOpen(true)
                showCountdownDialog()
            })
            .catch(err => showSnackBar(err.toString()))
    }

    return (
        <Paper className={classes.plugin}>
            <DownloadDialog open={dialogOpen} message={dialogMsg}/>
            <Box display='flex' flexDirection='row' justifyContent='flex-end' width='100%'>
                <IconButton size={'small'} onClick={handlePlugin}>
                    {statusConfig[status || 'default'].icon}
                </IconButton>
                {status === '版本不兼容' && <IconButton size={'small'} onClick={handelDelete}>
                    <DeleteIcon fontSize="small"/>
                </IconButton>}
            </Box>
            <Box display='flex' flexDirection='column' alignItems='baseline' width='100%' pr={4} pt={1}>
                <Typography>包名： {plugin.packageName}</Typography>
                <Typography variant='subtitle2'>插件名称： {plugin.pluginName}</Typography>
                <Typography variant='subtitle2'>插件版本： {plugin.pluginVersion}</Typography>
            </Box>
            <Box display='flex' flexDirection='column' alignItems='flex-end' width='100%'>
                <Chip size="small" label={status}
                      color={statusConfig[status || 'default'].color}/>
            </Box>
        </Paper>
    )
}

export default function PluginCenter () {
    const [allPlugins, setAllPlugins] = useState([])
    const [loading, setLoading] = useState(false)

    const {globalDispatch} = useContext(GlobalContext)

    const classes = useStyles()

    useEffect(() => {
        getAllPlugins()
    }, [])

    const getAllPlugins = () => {
        setLoading(true)
        PluginApi.getAllPlugins().then(data => {
            setAllPlugins(data.data.plugins)
            setLoading(false)
        })
    }

    const refreshPlugins = () => {
        getAllPlugins()
        initPlugins(globalDispatch)
    }

    return (
        <div className={classes.root}>
            <Backdrop className={classes.backdrop} open={loading}>
                <CircularProgress size={50} color="inherit"/>
                <Typography variant={'h3'}>加载中...</Typography>
            </Backdrop>
            {allPlugins.map(plugin => (
                <PluginPaper key={plugin.packageName} plugin={plugin} refreshPlugins={refreshPlugins}/>
            ))}
            {!loading && <Tooltip title="刷新">
                <Fab color="primary" className={classes.fab} onClick={refreshPlugins}>
                    <RefreshIcon/>
                </Fab>
            </Tooltip>}
        </div>
    )
}