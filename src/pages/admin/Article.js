import React, { useContext, useEffect, useState } from 'react'
import makeStyles from '@mui/styles/makeStyles';
import Button from '@mui/material/Button'
import Avatar from '@mui/material/Avatar'
import {Link, useNavigate} from 'react-router-dom'
import Paper from '@mui/material/Paper'
import { ArticleApi } from '../../api/article'
import { GlobalContext } from '../../globalState/Context'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(5),
        [theme.breakpoints.down('xl')]: {
            padding: theme.spacing()
        },
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        justifyItems: 'center'
    },
    avatar: {
        backgroundColor: theme.palette.secondary.main
    },
    article: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column'

    },
    header: {
        display: 'inline-flex',
        padding: theme.spacing(3),
        paddingBottom: 0,
        justifyContent: 'space-between',
        flexWrap: 'wrap'
    },
    action: {
        display: 'inline-flex',
        paddingTop: 0,
        padding: theme.spacing(3),
        justifyContent: 'flex-end'
    }
}))

export default function Article (props) {
    const [articles, setArticles] = useState([])

    const navigate = useNavigate()

    const {showSnackBar} = useContext(GlobalContext)

    const classes = useStyles()

    useEffect(() => {
        getArticles()
    }, [])

    const getArticles = () => {
        ArticleApi.getArticle()
            .then(data => setArticles(data.articles))
    }

    const handleDelete = id => () => {
        ArticleApi.deleteArticle(id)
            .then(r => {
                getArticles()
                showSnackBar('删除成功')
            })
            .catch(err => showSnackBar('删除失败!' + err))
    }

    return (
        <div className={classes.root}>
            {articles.map(article => {
                return (
                    <Paper className={classes.article}>
                        <div className={classes.header}>
                            <Avatar aria-label="Recipe" className={classes.avatar}>
                                {article.tag}
                            </Avatar>
                            <span>{article.title}</span>
                            <span>分类ID {article.category_id}</span>
                            <span>创建时间 {article.CreatedAt}</span>
                            <span>修改时间 {article.UpdatedAt}</span>
                        </div>
                        <div className={classes.action}>
                            <Button size="small" color="primary"
                                    component={Link} to={'/article/' + article.ID}>预览</Button>
                            <Button size="small" color="primary"
                                    onClick={handleDelete(article.ID)}
                            >删除</Button>
                            <Button size="small" color="primary"
                                    onClick={() => navigate(`/articleEditor/${article.ID}`)}
                            >编辑</Button>
                        </div>
                    </Paper>)
            })}
        </div>
    )
}
