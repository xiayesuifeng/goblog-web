import React, { useContext, useState } from 'react'
import { IconButton, ListItemText, TextField } from '@mui/material'
import InputAdornment from '@mui/material/InputAdornment'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction'
import DoneIcon from '@mui/icons-material/Done'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'
import { CategoryApi } from '../../api/category'
import makeStyles from '@mui/styles/makeStyles';
import { GlobalContext } from '../../globalState/Context'

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(5),
        display: 'inline-flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        justifyItems: 'center'
    },
    avatar: {
        margin: 'auto',
        marginBottom: theme.spacing(3),
        height: 150,
        width: 150,
    },
}))

export default function Category () {
    const [category, setCategory] = useState('')
    const [editID, setEditID] = useState(-1)

    const {categories, globalDispatch,showSnackBar} = useContext(GlobalContext)

    const classes = useStyles()

    const getCategories = () => {
        CategoryApi.getCategory()
            .then(data => globalDispatch({type:'SET_CATEGORIES',data:data.categorys}))
    }

    const addCategory = () => {
        if (category !== '') {
            CategoryApi.addCategory(category)
                .then(() => {
                    showSnackBar('添加成功')
                    setCategory('')
                    getCategories()
                })
                .catch(err => showSnackBar(err.toString()))
        }
    }

    const delCategory = id => () => {
        CategoryApi.delCategory(id)
            .then(() => {
                showSnackBar('删除成功')
                getCategories()
            })
            .catch(err => showSnackBar(err.toString()))
    }

    const editCategory = () => {
        CategoryApi.editCategory(editID, category)
            .then(() => {
                showSnackBar('编辑成功')
                setCategory('')
                setEditID(-1)
                getCategories()
            })
            .catch(err => showSnackBar(err.toString()))
    }

    const handleEdit = category => () => {
        setEditID(category.ID)
        setCategory(category.name)
    }

    return (
        <div className={classes.root}>
            <TextField
                id="category"
                label='分类名'
                type="text"
                value={category}
                onChange={e => setCategory(e.target.value)}
                margin="normal"
                InputProps={{
                    endAdornment: <InputAdornment position="start">
                        {editID === -1 ?
                            <IconButton onClick={addCategory} size="large">
                                <AddIcon/>
                            </IconButton>
                            :
                            <IconButton onClick={editCategory} size="large">
                                <DoneIcon/>
                            </IconButton>
                        }
                    </InputAdornment>,
                }}
            />
            <List>
                {categories.map(category => {
                    return (
                        <ListItem>
                            <ListItemText
                                primary={category.name}
                                secondary={'ID:' + category.ID}
                            />
                            <ListItemSecondaryAction>
                                <IconButton onClick={handleEdit(category)} size="large">
                                    <EditIcon/>
                                </IconButton>
                                <IconButton onClick={delCategory(category.ID)} size="large">
                                    <DeleteIcon/>
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    );
                })}
            </List>
        </div>
    );
}