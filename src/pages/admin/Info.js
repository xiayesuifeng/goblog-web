import React, { useContext, useEffect, useState } from 'react'
import { TextField } from '@mui/material'
import axios from 'axios'
import FormControlLabel from '@mui/material/FormControlLabel'
import Switch from '@mui/material/Switch'
import Button from '@mui/material/Button'
import Avatar from '@mui/material/Avatar'
import makeStyles from '@mui/styles/makeStyles';
import { GlobalContext } from '../../globalState/Context'

const useStyles = makeStyles(theme => ({
    root: {
        display: 'inline-flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        justifyItems: 'center'
    },
    avatar: {
        margin: 'auto',
        marginBottom: theme.spacing(3),
        height: 150,
        width: 150,
    },
}))

export default function Info () {
    const [logo, setLogo] = useState('none')
    const [logoFile, setLogoFile] = useState(null)
    const [name, setName] = useState('')
    const [useCategory, setUseCategory] = useState(false)

    const {info, globalDispatch,showSnackBar} = useContext(GlobalContext)

    const classes = useStyles()

    useEffect(() => {
        setName(info.title)
        setLogo(info.logo)
        setUseCategory(info.useCategory)
    },[info])

    const getInfo = () => {
        axios.get('/api/info')
            .then(r => {
                let info = {
                    title: r.data.name,
                    useCategory: r.data.useCategory,
                    logo: r.data.logo
                }

                if (info.logo !== 'none') {
                    document.querySelector('link[rel=\'shortcut icon\']').href = '/api/logo'
                }

                globalDispatch({type: 'SET_INFO', data: info})
            })
    }

    const handleLogoChange = e => {
        if (e.target.files[0] === undefined)
            return

        setLogo(URL.createObjectURL(e.target.files[0]))
        setLogoFile(e.target.files[0])
    }

    const updateInfo = () => {
        axios.patch('/api/info', {
            name: name,
            useCategory: useCategory
        }).then(r => {
            if (r.status === 200 && r.data.code === 200) {
                showSnackBar('保存成功')

                getInfo()
            } else {
                showSnackBar('保存失败')
            }
        }).catch(err => showSnackBar('保存失败' + err.toString()))
    }

    const handleUpload = () => {
        if (logoFile !== null) {
            let data = new FormData()
            data.append('logo', logoFile)

            axios.put('/api/logo', data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(r => {
                if (r.status === 200 && r.data.code === 200) {
                    updateInfo()
                } else {
                    showSnackBar('logo保存失败')
                }
            })
        } else {
            updateInfo()
        }
    }

    return (
        <div className={classes.root}>
            <Avatar src={logo !== 'none' && logo} className={classes.avatar}/>
            <input type="file" onChange={handleLogoChange}/>
            <TextField
                label="博客名"
                margin="normal"
                value={name}
                onChange={e => setName(e.target.value)}
            />
            <FormControlLabel
                control={
                    <Switch
                        checked={useCategory}
                        onChange={e => setUseCategory(e.target.checked)}
                        color="primary"
                    />
                }
                label="分类"
            />
            <Button variant="outlined" color="primary" onClick={handleUpload}>保存</Button>
        </div>
    )
}