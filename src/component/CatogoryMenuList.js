import { Divider, ListItemText, MenuItem, MenuList } from '@mui/material'
import { Link } from 'react-router-dom'
import React, { useContext } from 'react'
import { GlobalContext } from '../globalState/Context'

export default function CategoryMenuList (props) {
    const {categories} = useContext(GlobalContext)

    return (
        <div>
            <Divider/>
            <MenuList>
                <MenuItem component={Link} to='/'>
                    <ListItemText primary="全部分类"/>
                </MenuItem>
                {categories.map((c) => {
                    return (
                        <MenuItem key={c.ID} selected={props.category === c.ID} component={Link}
                                  to={'/category/' + c.ID}>
                            <ListItemText primary={c.name}/>
                        </MenuItem>
                    )
                })}
            </MenuList>
        </div>
    )
}
