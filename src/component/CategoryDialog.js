import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import { Button, IconButton, ListItemText, TextField } from '@mui/material'
import InputAdornment from '@mui/material/InputAdornment'
import AddIcon from '@mui/icons-material/Add'
import DoneIcon from '@mui/icons-material/Done'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'
import DialogActions from '@mui/material/DialogActions'
import React, { useContext, useState } from 'react'
import { CategoryApi } from '../api/category'
import { GlobalContext } from '../globalState/Context'

export default function CategoryDialog (props) {
    const [categoryData, setCategoryData] = useState({name: '', id: -1})

    const {categories, globalDispatch, showSnackBar} = useContext(GlobalContext)

    const getCategories = () => {
        CategoryApi.getCategory().then(data => globalDispatch({
            type: 'SET_CATEGORIES',
            data: data.categorys
        }))
    }

    const addCategory = () => {
        if (categoryData.name !== '') {
            CategoryApi.addCategory(categoryData.name)
                .then(() => {
                    showSnackBar('添加成功')
                    setCategoryData({name: '', id: -1})
                    getCategories()
                })
                .catch(err => showSnackBar(err.toString()))
        }
    }

    const delCategory = id => () => {
        CategoryApi.delCategory(id)
            .then(() => {
                showSnackBar('删除成功')
                getCategories()
            })
            .catch(err => showSnackBar(err.toString()))
    }

    const editCategory = () => {
        CategoryApi.editCategory(categoryData.id, categoryData.name)
            .then(() => {
                showSnackBar('编辑成功')
                setCategoryData({name: '', id: -1})
                getCategories()
            })
            .catch(err => showSnackBar(err.toString()))
    }

    const handleEdit = category => () => setCategoryData({id: category.ID, name: category.name})

    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}
            aria-labelledby="dialog-title"
        >
            <DialogTitle id="dialog-title">{'分类管理'}</DialogTitle>
            <DialogContent>
                <TextField
                    id="category"
                    label='分类名'
                    type="text"
                    value={categoryData.name}
                    onChange={e => setCategoryData({...categoryData, name: e.target.value})}
                    margin="normal"
                    InputProps={{
                        endAdornment: <InputAdornment position="start">
                            {categoryData.id === -1 ?
                                <IconButton onClick={addCategory} size="large">
                                    <AddIcon/>
                                </IconButton>
                                :
                                <IconButton onClick={editCategory} size="large">
                                    <DoneIcon/>
                                </IconButton>
                            }
                        </InputAdornment>,
                    }}
                />
                <List>
                    {categories.map(category => {
                            return (
                                <ListItem key={category.ID}>
                                    <ListItemText
                                        primary={category.name}
                                        secondary={'ID:' + category.ID}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton onClick={handleEdit(category)} size="large">
                                            <EditIcon/>
                                        </IconButton>
                                        <IconButton onClick={delCategory(category.ID)} size="large">
                                            <DeleteIcon/>
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            );
                        }
                    )}
                </List>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClose} color="primary"
                        autoFocus>
                    关闭
                </Button>
            </DialogActions>
        </Dialog>
    );
}
