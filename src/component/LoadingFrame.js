import React from 'react'
import CircularProgress from '@mui/material/CircularProgress'
import Typography from '@mui/material/Typography'
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
    }
}))

export default function LoadingFrame () {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <CircularProgress size={200}/>
            <Typography>加载中...</Typography>
        </div>
    )
}