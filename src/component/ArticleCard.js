import { Avatar, Button, Card, CardActions, CardContent, CardHeader, IconButton } from '@mui/material'
import Clipboard from 'react-clipboard.js'
import TuiEditorViewer from './TuiEditorViewer'
import { Link } from 'react-router-dom'
import React, {useContext, useEffect, useState} from 'react'
import makeStyles from '@mui/styles/makeStyles';
import ShareIcon from '@mui/icons-material/Share'
import Skeleton from '@mui/material/Skeleton'
import Box from '@mui/material/Box'
import { GlobalContext } from '../globalState/Context'
import axios from "axios";

const useStyles = makeStyles(theme => ({
    avatar: {
        backgroundColor: theme.palette.secondary.main
    },
    card: {
        textAlign: 'left',
        margin: theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            margin: theme.spacing(10),
        },
    },
    cardAction: {
        position: 'relative'
    },
    readArticle: {
        position: 'absolute',
        right: 10
    },
}))

function ArticleCard (props) {
    const classes = useStyles()

    const {loading = false,article,categories} = props

    const {info,showSnackBar} = useContext(GlobalContext)

    const [desc,setDesc] = useState('')

    useEffect(() => {
        if (article) {
            axios.get('/api/article/uuid/' + article.Uuid + '/description_md')
                .then(r => {
                    if (r.data.code === 200)
                        setDesc(r.data.markdown)
                })
        }
    },[article])

    const getCategory = id => {
        for (let i = 0; i < categories.length; i++) {
            if (categories[i].ID === id)
                return categories[i].name
        }
        return '已删除的分类'
    }

    const formatTime = time => {
        time = time.replace('T', ' ')
        time = time.replace('Z', '')
        return new Date(time).toLocaleString()
    }

    return (
        <Card className={classes.card}>
            <CardHeader
                avatar={
                    loading ? <Skeleton variant="circular" width={40} height={40}/>
                            : <Avatar aria-label="Recipe" className={classes.avatar}>
                                {article.tag}
                            </Avatar>
                }
                action={
                    !loading && <Clipboard
                        component='div'
                        data-clipboard-text={window.location.protocol + '//' + window.location.host + '/article/' + article.ID}
                        onSuccess={() => showSnackBar('已复制到剪切板')}>
                        <IconButton size="large">
                            <ShareIcon/>
                        </IconButton>
                    </Clipboard>
                }
                title={loading ? <Skeleton height={8} width="80%" /> : article.title}
                subheader={loading ? <Skeleton height={8} width="40%" /> : formatTime(article.CreatedAt)}
            />
            <CardContent>
                {loading || !desc ? <Box>
                        <Skeleton/>
                        <Skeleton/>
                        <Skeleton width="80%"/>
                    </Box>
                    : <TuiEditorViewer value={desc}/>}
            </CardContent>
            {!loading && <CardActions className={classes.cardAction}>
                {info.useCategory &&
                <Button size="small" color="primary" component={Link}
                        to={'/category/' + article.category_id}>归类于 {getCategory(article.category_id)}</Button>}
                <Button size="small" color="primary"
                        className={info.useCategory && classes.readArticle}
                        component={Link} to={'/article/' + article.ID}>阅读全文</Button>
            </CardActions>}
        </Card>
    );
}

export default ArticleCard
