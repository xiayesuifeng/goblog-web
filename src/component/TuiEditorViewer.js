import React, { Component } from 'react'
import 'codemirror/lib/codemirror.css'
import '@toast-ui/editor/dist/toastui-editor-viewer.css'
import 'highlight.js/styles/github.css'
import Viewer from '@toast-ui/editor/dist/toastui-editor-viewer'
import hljs from 'highlight.js'
import codeSyntaxHighlight from '@toast-ui/editor-plugin-code-syntax-highlight'
import table from '@toast-ui/editor-plugin-table-merged-cell'

class TuiEditorViewer extends Component {
    constructor (props, context) {
        super(props, context)
        this.viewerRef = React.createRef()
    }

    componentDidMount () {
        let viewer = new Viewer({
            el: this.viewerRef.current,
            height: '100%',
            plugins: [table, [codeSyntaxHighlight, {hljs}]],
        })
        this.setState({viewer})
    }

    shouldComponentUpdate (nextProps, nextState, nextContext) {
        nextState.viewer.setMarkdown(nextProps.value)
        return true
    }

    render () {
        return (
            <div ref={this.viewerRef}/>
        )
    }
}

export default TuiEditorViewer
